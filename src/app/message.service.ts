import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor() { }

  messages: string[] = [];// this property [messages] can only receive an array[] of strings and initializing it as an empty array.

  add(message: string) {
    this.messages.push(message);
  }

  clear() {
    this.messages = [];
  }
}
